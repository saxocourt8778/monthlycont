﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CompPlugin;

namespace MonthlyCont {
    public class MonthlyComp : Plugin {
        // Provide an actual name
        public override string Name {
            get {
                return "Monthly";
            }
        }

        // Provide an actual function to calc int earned in one year w/ ann comp
        public override double dCalcInt(double dPrinciple, double dIntRate) {
            return dPrinciple * ((Math.Pow((1 + dIntRate / 12), 12) - 1));
        }

    }



    public class ContinuousComp : Plugin {
        // Provide an actual name
        public override string Name {
            get {
                return "Continuous";
            }
        }

        // Provide an actual function to calc int earned in one year w/ ann comp
        public override double dCalcInt(double dPrinciple, double dIntRate) {
            return dPrinciple * (Math.Exp(dIntRate) - 1);
        }

    }
}
